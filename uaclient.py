# !/usr/bin/python3
# -*-coding: utf-8-*-

import os
import sys
import time
import socket
from hashlib import md5
from uaserver import XMLHandler
from proxy_registrar import Log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class UAClient:

    def __init__(self, config):
        self.tags = self.parse_config(config)
        self.message = ''
        self.log = Log(self.tags['log_path'])
        self.log.starting()

    def send_message(self, method, option=''):
        if method.lower() == 'register':
            self.register(option)
        if method.lower() == 'invite':
            self.invite(option)
        if method.lower() == 'bye':
            self.bye(option)
        data = self.send()
        if '401' in data and method.lower() == 'register':
            self.add_digest(data)
            data = self.send()
        if '100' in data and '180' in data and '200' in data:
            self.ack(option)
            mp32rtp, cvlc = self.get_commands(data)
            data = self.send(False)
            print('running: ' + mp32rtp + ' && ' + cvlc)
            os.system(mp32rtp + ' && ' + cvlc)

    def get_commands(self, data):
        ip = data.split('\r\n')[6].split(' ')[1]
        port = data.split('\r\n')[9].split(' ')[1]
        mp32rtp = './mp32rtp -i ' + ip + ' -p ' + port
        mp32rtp += ' < ' + self.tags['audio_path']
        cvlc = 'cvlc rtp://@' + ip + ':' + port + ' 2> /dev/null'
        return mp32rtp, cvlc

    def register(self, option):
        try:
            i = int(option)
        except:
            self.log.error('Option must be a number')
            sys.exit('invalid option, expires must be a number')
        self.message = 'REGISTER sip:' + self.tags['account_username']
        self.message += ':' + self.tags['uaserver_puerto'] + ' SIP/2.0\r\n'
        self.message += 'Expires: ' + option + '\r\n'

    def add_digest(self, data):
        digest = md5()
        digest.update(bytes(data.split('"')[1], 'utf-8'))
        digest.update(bytes(self.tags['account_passwd'], 'utf-8'))
        digest.digest()
        self.message += 'Authorization: Digest response="'
        self.message += digest.hexdigest() + '"\r\n'

    def invite(self, option):
        self.message = 'INVITE sip:' + option + ' SIP/2.0\r\n'
        self.message += 'Content-Type: application/sdp\r\n\r\n'
        self.message += 'v=0\r\n'
        self.message += 'o=' + self.tags['account_username']
        self.message += ' ' + self.tags['uaserver_ip'] + '\r\n'
        self.message += 's=misesion\r\n'
        self.message += 't=0\r\n'
        self.message += 'm=audio ' + self.tags['rtpaudio_puerto'] + ' RTP\r\n'

    def ack(self, option):
        self.message = 'ACK sip:' + option + ' SIP/2.0\r\n'

    def bye(self, option):
        self.message = 'BYE sip:' + option + ' SIP/2.0\r\n'

    def send(self, receive=True):
        ip = self.tags['regproxy_ip']
        port = int(self.tags['regproxy_puerto'])
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, port))
            my_socket.send(bytes(self.message, 'utf-8'))
            address = ip + ':' + str(port)
            self.log.sent_to(address, self.message)
            if receive:
                try:
                    data = my_socket.recv(1024).decode('utf-8')
                    if data:
                        print(data)
                        self.log.received_from(address, data)
                    return data
                except:
                    self.log.error('Connection refused')
                    sys.exit('connection refused')
            else:
                return ''

    def parse_config(self, config):
        parser = make_parser()
        xml_list = XMLHandler()
        parser.setContentHandler(xml_list)
        parser.parse(open(config))
        return xml_list.get_tags()


def get_arguments():
    if len(sys.argv) != 4:
        sys.exit('usage error: python uaclient.py <config><method><option>')
    return sys.argv[1], sys.argv[2], sys.argv[3]

if __name__ == '__main__':

    config, method, option = get_arguments()
    if os.path.exists(config):
        UA = UAClient(config)
    else:
        sys.exit('file ' + config + ' not found')
    UA.send_message(method, option)
    UA.log.finishing()
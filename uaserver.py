# !/usr/bin/python3
# -*-coding: utf-8-*-

import os
import sys
import time
import socketserver
from proxy_registrar import Log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class XMLHandler(ContentHandler):

    def __init__(self):
        self.conf = {}
        self.att = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        if name in self.att:
            for att in self.att[name]:
                self.conf[name + "_" + att] = attrs.get(att, '')

    def get_tags(self):
        return self.conf


class ServerHandler(socketserver.DatagramRequestHandler):

    rtp = []

    def handle(self):
        data = self.rfile.read().decode('utf-8')
        address = self.client_address[0] + ':' + str(self.client_address[1])
        log.received_from(address, data)
        print(data)
        if 'INVITE' in data:
            self.wfile.write(bytes(self.get_100(), 'utf-8'))
            log.sent_to(address, self.get_100())
            time.sleep(0.5)
            self.wfile.write(bytes(self.get_180(), 'utf-8'))
            log.sent_to(address, self.get_180())
            time.sleep(0.5)
            self.wfile.write(bytes(self.get_200() + self.sdp_body(), 'utf-8'))
            log.sent_to(address, self.get_200() + self.sdp_body())
            self.get_commands(data)
        elif 'ACK' in data:
            mp32rtp = './mp32rtp -i ' + self.rtp[0] + ' -p ' + self.rtp[1]
            mp32rtp += ' < ' + tags['audio_path']
            cvlc = 'cvlc rtp://@' + self.rtp[0] + ':' + self.rtp[1]
            cvlc += ' 2> /dev/null'
            print('running: ' + mp32rtp + ' && ' + cvlc)
            os.system(mp32rtp + ' && ' + cvlc)
        elif 'BYE' in data:
            self.rtp = []
            self.wfile.write(bytes(self.get_200(), 'utf-8'))
            log.sent_to(address, self.get_200())

    def get_commands(self, data):
        ip = data.split('\r\n')[4].split(' ')[1]
        port = data.split('\r\n')[7].split(' ')[1]
        self.rtp.append(ip)
        self.rtp.append(port)

    def sdp_body(self):
        mess = 'Content-Type: application/sdp\r\n\r\n'
        mess += 'v=0\r\n'
        mess += 'o=' + tags['account_username'] + ' ' + tags['uaserver_ip']
        mess += '\r\ns=misesion\r\n'
        mess += 't=0\r\n'
        mess += 'm=audio ' + tags['rtpaudio_puerto'] + ' RTP\r\n'
        return mess

    def get_405(self):
        mess = 'SIP/2.0 405 Method Not Allowed\r\n'
        return mess

    def get_404(self):
        mess = 'SIP/2.0 404 User Not Found\r\n'
        return mess

    def get_401(self, username):
        mess = 'SIP/2.0 401 Unauthorized\r\nWWW Authenticate: Digest nonce="'
        mess += str(self.nonce[username]) + '"\r\n'
        return mess

    def get_400(self):
        mess = 'SIP/2.0 400 Bad Request\r\n'
        return mess

    def get_200(self):
        mess = 'SIP/2.0 200 OK\r\n'
        return mess

    def get_180(self):
        mess = 'SIP/2.0 180 Ringing\r\n'
        return mess

    def get_100(self):
        mess = 'SIP/2.0 100 Trying\r\n'
        return mess


def parse_config(config):
    parser = make_parser()
    xml_list = XMLHandler()
    parser.setContentHandler(xml_list)
    parser.parse(open(config))
    return xml_list.get_tags()


def get_arguments():

    if len(sys.argv) != 2:
        sys.exit('usage error: python3 uaserver.py <config>')
    return sys.argv[1]

if __name__ == '__main__':

    config = get_arguments()
    if os.path.exists(config):
        tags = parse_config(config)
    else:
        sys.exit('file ' + config + ' not found')
    log = Log(tags['log_path'])
    ip = tags['uaserver_ip']
    port = int(tags['uaserver_puerto'])
    server = socketserver.UDPServer((ip, port), ServerHandler)
    print('Listening at ' + ip + ':' + str(port) + '...\r\n')
    try:
        log.starting()
        server.serve_forever()
    except KeyboardInterrupt:
        log.finishing()
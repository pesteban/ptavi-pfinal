# !/usr/bin/python3
# -*-coding: utf-8-*-

import os
import sys
import json
import time
import random
import socket
import socketserver
from hashlib import md5
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class Log:

    def __init__(self, log_file):
        self.file = log_file
        self.date = '%Y%m%d%H%M%S'

    def sent_to(self, address, mess):
        now = time.gmtime(time.time() + 3600)
        with open(self.file, 'a') as log:
            mess = time.strftime((self.date), now)
            mess += ' Sent to ' + address
            mess += ': ' + mess.replace('\r\n', ' ') + '\n'
            log.write(mess)

    def received_from(self, address, mess):
        now = time.gmtime(time.time() + 3600)
        with open(self.file, 'a') as log:
            mess = time.strftime((self.date), now)
            mess += ' Received from ' + address
            mess += ': ' + mess.replace('\r\n', ' ') + '\n'
            log.write(mess)

    def error(self, mess):
        now = time.gmtime(time.time() + 3600)
        with open(self.file, 'a') as log:
            mess = time.strftime((self.date), now)
            mess += ' Error: ' + mess + '\n'
            log.write(mess)

    def starting(self):
        now = time.gmtime(time.time() + 3600)
        with open(self.file, 'a') as log:
            mess = time.strftime((self.date), now)
            mess += ' Starting...\n'
            log.write(mess)

    def finishing(self):
        now = time.gmtime(time.time() + 3600)
        with open(self.file, 'a') as log:
            mess = time.strftime((self.date), now)
            mess += ' Finishing.\n'
            log.write(mess)


class XMLHandler(ContentHandler):

    def __init__(self):
        self.conf = {}
        self.att = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'passwdpath'],
                    'log': ['path']}

    def startElement(self, name, attrs):
        if name in self.att:
            for att in self.att[name]:
                self.conf[name + "_" + att] = attrs.get(att, '')

    def get_tags(self):
        return self.conf


class SIPHandler(socketserver.DatagramRequestHandler):

    reg = {}
    pwd = {}
    nonce = {}

    def json2register(self):
        try:
            with open(tags['database_path'], 'r') as jsonfile:
                self.reg = json.load(jsonfile)
        except:
            pass

    def json2passwd(self):
        try:
            with open(tags['database_passwdpath'], 'r') as jsonfile:
                self.pwd = json.load(jsonfile)
        except:
            pass

    def register2json(self):
        with open(tags['database_path'], 'w') as jsonfile:
            json.dump(self.reg, jsonfile, indent=3)

    def passwd2json(self):
        with open(tags['database_passwdpath'], 'w') as jsonfile:
            json.dump(self.pwd, jsonfile, indent=3)

    def handle(self):
        self.json2register()
        self.json2passwd()
        address = self.client_address[0] + ':' + str(self.client_address[1])
        data = self.rfile.read().decode('utf-8')
        log.received_from(address, data)
        print(data)
        if 'REGISTER' in data:
            user_src, expires, port, registered = self.register_mess(data)
            if not registered:
                if user_src in self.nonce:
                    response = data.split('"')[1]
                    if response == self.get_digest_response(user_src):
                        self.add_client(user_src, port, expires)
                        self.wfile.write(bytes(self.get_200(), 'utf-8'))
                        log.sent_to(address, self.get_200())
                        del self.nonce[user_src]
                    else:
                        pass
                else:
                    if expires != 0:
                        n = random.randint(0000000000, 9999999999)
                        self.nonce[user_src] = n
                        mess = self.get_401(user_src)
                        self.wfile.write(bytes(mess, 'utf-8'))
                        log.sent_to(address, mess)
            else:
                if int(expires) == 0:
                    self.delete_client(user_src)
                else:
                    self.add_client(user_src, port, expires)
                self.wfile.write(bytes(self.get_200(), 'utf-8'))
                log.sent_to(address, self.get_200())
        elif 'INVITE' in data:
            user_dst, user_src = self.invite_mess(data)
            if user_src in self.reg and user_dst in self.reg:
                reply = self.resent(user_dst, data)
                if reply:
                    self.wfile.write(bytes(reply, 'utf-8'))
                    log.sent_to(address, reply)
            else:
                self.wfile.write(bytes(self.get_404(), 'utf-8'))
                log.sent_to(address, self.get_404())
        elif 'BYE' in data:
            user_dst = self.bye_mess(data)
            reply = self.resent(user_dst, data)
            if reply:
                self.wfile.write(bytes(reply, 'utf-8'))
                log.sent_to(address, reply)
        elif 'ACK' in data:
            user_dst = self.ack_mess(data)
            reply = self.resent(user_dst, data)
            if reply:
                self.wfile.write(bytes(reply, 'utf-8'))
                log.sent_to(address, reply)
        else:
            self.wfile.write(bytes(self.get_405(), 'utf-8'))
            log.sent_to(address, self.get_405())
        self.passwd2json()
        self.register2json()

    def resent(self, user_dst, data):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect(('127.0.0.1', self.reg[user_dst][0]))
            my_socket.send(bytes(data, 'utf-8'))
            address = '127.0.0.1:' + str(self.reg[user_dst][0])
            log.sent_to(address, data)
            try:
                reply = my_socket.recv(1024).decode('utf-8')
                if reply:
                    print(reply)
                    log.received_from(address, reply)
                return reply
            except:
                return ''

    def get_digest_response(self, username):
        digest = md5()
        digest.update(bytes(str(self.nonce[username]), 'utf-8'))
        digest.update(bytes(self.pwd[username], 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def get_405(self):
        mess = 'SIP/2.0 405 Method Not Allowed\r\n'
        return mess

    def get_404(self):
        mess = 'SIP/2.0 404 User Not Found\r\n'
        return mess

    def get_401(self, username):
        mess = 'SIP/2.0 401 Unauthorized\r\nWWW Authenticate: Digest nonce="'
        mess += str(self.nonce[username]) + '"\r\n'
        return mess

    def get_400(self):
        mess = 'SIP/2.0 400 Bad Request\r\n'
        return mess

    def get_200(self):
        mess = 'SIP/2.0 200 OK\r\n'
        return mess

    def get_180(self):
        mess = 'SIP/2.0 180 Ringing\r\n'
        return mess

    def get_100(self):
        mess = 'SIP/2.0 100 Trying\r\n'
        return mess

    def add_client(self, username, port, expires):
        t = time.gmtime(time.time() + 3600 + expires)
        exp = time.strftime('%d/%m/%Y %H:%M:%S', t)
        self.reg[username] = [port, exp]

    def delete_client(self, username):
        del self.reg[username]

    def register_mess(self, data):
        username = data.split(':')[1]
        port = data.split(':')[2].split(' ')[0]
        expires = data.split('\r\n')[1].split(':')[1]
        return username, int(expires), int(port), username in self.reg

    def invite_mess(self, data):
        user_dst = data.split(':')[1].split(' ')[0]
        user_src = data.split('\r\n')[4].split('=')[1].split(' ')[0]
        return user_dst, user_src

    def bye_mess(self, data):
        username = data.split(':')[1].split(' ')[0]
        return username

    def ack_mess(self, data):
        username = data.split(':')[1].split(' ')[0]
        return username


def parse_config(config):
    parser = make_parser()
    xml_list = XMLHandler()
    parser.setContentHandler(xml_list)
    parser.parse(open(config))
    return xml_list.get_tags()


def get_arguments():
    if len(sys.argv) != 2:
        sys.exit('usage error: python3 uaserver.py <config>')
    return sys.argv[1]

if __name__ == '__main__':

    config = get_arguments()
    if os.path.exists(config):
        tags = parse_config(config)
    else:
        sys.exit('file ' + config + ' not found')
    log = Log(tags['log_path'])
    ip = tags['server_ip']
    port = int(tags['server_puerto'])
    proxy = socketserver.UDPServer((ip, port), SIPHandler)
    name = tags['server_name']
    print('Server ' + name + ' listening at ' + ip + ':' + str(port) + '...\n')
    try:
        log.starting()
        proxy.serve_forever()
    except KeyboardInterrupt:
        log.finishing()
        print('\nEnd ' + tags['server_name'])